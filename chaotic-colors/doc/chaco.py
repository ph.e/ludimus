from IPython.core.display import display, HTML
import random
from copy import copy

color_code = {
    "r": "#FF0000",
    "g": "#00FF00",
    "b": "#0000FF",
    "p": "#FFFF00",
    "v": "#FF00FF",
    "x": "#00FFFF",
    "y": "#FF00FF",
}


class Game:
    """
    cell properties :
    * x : 0-based column coordinate
    * y : 0-based row coordinate
    * color : color id (mandatory)
    * owner : player id
    * pass : flag for multiple pass
        = 0 : init, not processed yet
        > 0 : processed
        = 1 : processed, useless
        = 2 : processed, usefull
    * claim : dict for player claim score
        player: 0 : no claim for this cell from player

    """

    def __init__(
        self, players=None, colors=None, board_size=None, seed=None, resume=None
    ):
        random.seed(seed)

        if resume is not None:
            assert board_size is None
            assert players is None
            assert colors is None

            rows = resume.strip().split("\n")
            rows = [
                [
                    [cell[0], cell[1].strip() if len(cell) > 0 else None]
                    for cell in row.strip().split(",")
                    if len(cell) > 1
                ]
                for row in rows
            ]
            rows = [
                [{"color": c, "owner": p if p != "" else None} for [c, p] in row]
                for row in rows
            ]

            board_size = (max([len(row) for row in rows]), len(rows))
            print("board_size", board_size)
            players = []
            colors = []
            for row in rows:
                for cell in row:
                    color, player = cell["color"], cell.get("owner")
                    if color not in colors:
                        colors.append(color)
                    if player not in players and player is not None:
                        players.append(player)
            print("players", players)

        else:
            if board_size is None:
                board_size = (8, 8)

            rows = []
            for y in range(board_size[1]):
                row = []
                for x in range(board_size[0]):
                    row.append({"color": random.choice(colors)})
                rows.append(row)

        # set x,y coordinates
        for y in range(board_size[1]):
            for x in range(board_size[0]):
                rows[y][x]["x"] = x
                rows[y][x]["y"] = y

        assert 0 < len(players) < 6
        assert 1 < len(colors) < 6
        assert 2 < board_size[0] < 20
        assert 2 < board_size[1] < 20

        self.players = players
        self.colors = colors
        self.board_size = board_size
        self.board = rows

        if resume is None:
            for player in players:
                self.random_position(player)

    def __repr__(self):
        res = ""
        for y in range(self.board_size[1]):
            for x in range(self.board_size[0]):
                res = res + self.board[y][x]["color"] + " "
            res += "\n"
        return res

    def _repr_html_(self):
        def make_row(cols):
            cols_html = ""
            for col in cols:
                color = color_code.get(col["color"], "#FFFFFF")
                player = col.get("owner") if col.get("owner") is not None else "◻️"
                cols_html = cols_html + f'<td bgcolor="{color}">{player}</td>'
            # cols_html = ''.join([f'<td bgcolor="#FF0000">{col}</td>' for col in cols])
            row = f"<tr>{cols_html}</tr>"
            return row

        header = make_row(
            [{"color": None, "owner": ""}]
            + [{"color": None, "owner": x} for x in range(self.board_size[0])]
        )
        rows = ""
        for y in range(self.board_size[1]):
            rows = rows + make_row([{"color": None, "owner": y}] + self.board[y])
        html = f"<table>{header}{rows}</table>"
        return html

    def random_position(self, player):
        while True:
            x = random.randrange(self.board_size[0])
            y = random.randrange(self.board_size[1])
            if self.board[y][x].get("owner") is None:
                self.board[y][x]["owner"] = player
                return (x, y)

    def adjacent_cell(self, cell, direction):
        cell_x = cell["x"] + direction[0]
        cell_y = cell["y"] + direction[1]
        if 0 <= cell_x < self.board_size[0] and 0 <= cell_y < self.board_size[1]:
            return self.board[cell_y][cell_x]

    def adjacent_conquest(self, player, cell):
        """
        Find adjacent cells of *cell* with the same color
        add them to cell claim for that player
        An existing claim for this player means 
        """

        color = cell["color"]
        recurs = []
        for direction in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
            cell2 = self.adjacent_cell(cell, direction)
            if cell2 is not None:
                if cell2["color"] == color:
                    # Found adjacent potential conquest from current cell
                    # first claim at all (any player)
                    if "claim" not in cell2:
                        cell2["claim"] = {}
                    # first claim for this player
                    if player not in cell2["claim"]:
                        cell2["claim"][player] = 0
                    # Increase claim, may be > 1
                    cell2["claim"][player] += 1
                    # Recursive call on this cell only once per player
                    if cell2["claim"][player] == 1:
                        recurs.append(cell2)
        # recursive calls at the end
        for cell2 in recurs:
            # Recursive call for next conquests from current conquest
            self.adjacent_conquest(player, cell2)

    def board_iter(self):
        """
        Simple iterator over all cells of the board
        """
        for y in range(self.board_size[1]):
            for x in range(self.board_size[0]):
                yield self.board[y][x]

    def move(self, color):
        # reset claim
        for cell in self.board_iter():
            cell["claim"] = {}

        # Find all cells already owned
        for cell in self.board_iter():
            player = cell.get("owner")
            # Only start conquest from cell already owned
            if player is not None:
                cell["color"] = color
                self.adjacent_conquest(player, cell)

        # We have now filled all claims for each cell and player
        # change cell ownership from claim
        # raise Exception("Refactoring in progres ...")
        for cell in self.board_iter():
            if "claim" in cell:
                claims = cell["claim"]
                if len(claims) > 0:
                    winner, max_claim = None, 0
                    for player in claims:
                        claim = claims[player]
                        if claim >= max_claim:
                            # TODO: draw case
                            winner, max_claim = player, claim
                    cell["owner"] = winner
                del cell["claim"]
        return self
