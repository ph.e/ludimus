from IPython.core.display import display, HTML
import random
from copy import copy

color_code = {
    'r': '#FF0000',
    'g': '#00FF00',
    'b': '#0000FF',
    'p': '#FFFF00',
    'v': '#FF00FF',
    'x': '#00FFFF',
    'y': '#FF00FF',
}


            def random_fill(self, colors, seed=None):
                rows = []
                for y in range(self.board_size[1]):
                    row = []
                    for x in range(self.board_size[0]):
                        row.append(random.choice(colors))
                    rows.append(row)
                return rows

class Board():
    '''
    
    '''
    def __init__(self, board_size=(8,8)):
        assert 5 < board_size[0] < 20
        assert 5 < board_size[1] < 20

        self.board_size = board_size
        self.board = self.random_fill(colors=[''], seed=None)
        self.territory = self.random_fill(colors=[''], seed=None)
     
        
    def __repr__(self):
        res = ''
        for y in range(self.board_size[1]):
            for x in range(self.board_size[0]):
                res = res + self.board[y][x] + ' '
            res += '\n'
        return res
    
    def _repr_html_(self):
        def make_row(cols):
            cols_html = ''
            for col in cols:
                color = color_code.get(col, '#FFFFFF')
                cols_html = cols_html + f'<td bgcolor="{color}">{col}</td>'
            #cols_html = ''.join([f'<td bgcolor="#FF0000">{col}</td>' for col in cols])
            row = f'<tr>{cols_html}</tr>'
            return row
        
        header = make_row([''] + list(range(self.board_size[0])))
        rows = ''
        for y in range(self.board_size[1]):            
            rows = rows + make_row([y] + self.board[y])
        html = f'<table>{header}{rows}</table>'
        return html
    

        
    def random_position(self, playe):
        while True:
            x = random.randrange(self.board_size[0])
            y = random.randrange(self.board_size[1])
            if self.territory[y][x] == '':
                self.territory[y][x] = playe
                return (x, y)
        
            
class Game():
    def __init__(self, players, colors, board_size=(8,8)):
        assert 0 < len(players) < 6
        assert 1 < len(colors) < 6
        
        self.players = players        
        self.colors = colors
        self.board = Board(board_size)
        self.board = self.board.random_fill(colors)
        
        for player in players:
            self.board.random_position(player)
        
    def __repr__(self):
        return self.board.__repr__()

    def _repr_html_(self):
        return self.board._repr_html_()

    def move(selg, player, color):
        raise "Not implemented yet !"