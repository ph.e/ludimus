extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var sizeX = 8
var sizeY = 8

var color = [
	Color(1.0,0.0,0.0),
	Color(0.0,0.0,1.0),
	Color(0.0,1.0,1.0),
	]

func _draw():
	# Your draw commands here
	var rect = [Rect2(Vector2(50,50),Vector2(200,200)),Rect2(Vector2(150,200),Vector2(200,200))]
	for x in range(sizeX):
		for y in range(sizeY):
			draw_rect(Rect2(Vector2(x*(50+5),y*(50+5)),Vector2(50,50)),color[1])