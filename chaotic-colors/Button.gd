extends Button

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# cf https://godotdevelopers.org/forum/discussion/18835/beginner-how-to-instance-a-grid-of-2d-sprites

onready var cell_scene = preload("res://cell.tscn")
#onready var grid_container = get_node("/VBoxContainer/GridContainer")
onready var grid_container = get_node("../../GridContainer")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	var cell = cell_scene.instance()
	cell.position = Vector2(100, 100)
	grid_container.add_child(cell)