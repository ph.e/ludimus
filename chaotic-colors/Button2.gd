extends Button

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var cell_scene = preload("res://cell.tscn")
#onready var grid_container = get_node("/VBoxContainer/GridContainer")
onready var grid_container = get_node("../../PanelContainer/Node2D")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func spawn_cell(type, pos):
	var cell = cell_scene.instance()
	cell.position = pos
	grid_container.add_child(cell)	
	
func spawn_board():
	var i=0
	while (i<36):
		spawn_cell(randi()%6+1, Vector2((i%6)*32+32, (i/6)*32+32))
		i=i+1

func _on_Button2_pressed():
	spawn_board()