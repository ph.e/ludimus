

# Server

```
uvicorn main:app --reload
```

Go to :
* http://127.0.0.1:8000
* http://127.0.0.1:8000/docs
* http://127.0.0.1:8000/redoc