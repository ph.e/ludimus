FROM ubuntu:18.10

RUN apt update \
  && apt upgrade -y \
  && apt dist-upgrade -y \
  && rm -rf /var/lib/apt/lists/*

#RUN \
#  apt-get -y install libgl1-mesa-glx libgl1-mesa-dri && \
#  rm -rf /var/lib/apt/lists/*

# Tools
RUN apt update \
  && apt install -y \
  alsa-base \
  alsa-utils \
  byobu \
  glmark2 \
  pulseaudio \
  wget \
  && rm -rf /var/lib/apt/lists/*

# Games
RUN apt update \
  && apt install -y \
  netpanzer \
  neverball \
  neverputt \
  teeworlds \
  teeworlds-server \
  && rm -rf /var/lib/apt/lists/*


# ssh keys
#TODO: merge with tools section
RUN apt update \
  && apt install -y \
  ssh \
  unzip \
  && rm -rf /var/lib/apt/lists/*


# OpenArena 0.8.8
# Don't use apt install openarena because of this bug :
# https://bugs.launchpad.net/ubuntu/+source/openarena/+bug/1651561
RUN cd /tmp \
  && wget https://download.tuxfamily.org/openarena/rel/088/openarena-0.8.8.zip \
  && unzip openarena-0.8.8.zip \
  && mv openarena-0.8.8 /opt/ \
  && rm openarena-0.8.8.zip

# Blender 2.80beta
RUN cd /tmp \
  && wget https://builder.blender.org/download/blender-2.80-ec471a9b1c14-linux-glibc224-x86_64.tar.bz2 \
  && tar xjf blender-2.80-ec471a9b1c14-linux-glibc224-x86_64.tar.bz2 \
  && mv blender-2.80-ec471a9b1c14-linux-glibc224-x86_64 /opt/ \
  && rm blender-2.80-ec471a9b1c14-linux-glibc224-x86_64.tar.bz2

# Godot 3.0.6
RUN cd /tmp \
  && wget -nv --show-progress --progress=bar:force:noscroll \
  https://downloads.tuxfamily.org/godotengine/3.0.6/Godot_v3.0.6-stable_x11.64.zip \
  && unzip Godot_v3.0.6-stable_x11.64.zip \
  && mv Godot_v3.0.6-stable_x11.64 /opt/ \
  && rm Godot_v3.0.6-stable_x11.64.zip


WORKDIR /ludimus-node
CMD ./welcome

# Toblo 1.3
#TODO: move up !
RUN cd /tmp \
  && wget -nv --show-progress --progress=bar:force:noscroll \
  http://downloads.digipen.edu/arcade/downloads/035/Toblo_Setup_1.3.exe \
  && mv Toblo_Setup_1.3.exe /opt/

#TODO: move up in tools !
RUN apt update \
  && apt install -y \
  software-properties-common \
  && rm -rf /var/lib/apt/lists/*

# Wine staging
RUN apt update \
  && dpkg --add-architecture i386 \
  && wget -nc https://dl.winehq.org/wine-builds/winehq.key \
  && apt-key add winehq.key \
  && apt-add-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ cosmic main' \
  && apt update \
  && apt install -y --install-recommends winehq-staging \
  && rm -rf /var/lib/apt/lists/*

# Lutris
RUN ver=18.10 \
  && echo "deb http://download.opensuse.org/repositories/home:/strycore/xUbuntu_$ver/ ./" | tee /etc/apt/sources.list.d/lutris.list \
  && wget -q https://download.opensuse.org/repositories/home:/strycore/xUbuntu_$ver/Release.key -O- | apt-key add - \
  && apt update \
  && apt install -y lutris \
  && rm -rf /var/lib/apt/lists/*


RUN apt update \
  && apt install -y \
  iputils-ping \
  less \
  sshfs \
  vim \
  && rm -rf /var/lib/apt/lists/*

# liquidwar6 0.6
RUN cd /tmp \
  && wget -nv --show-progress --progress=bar:force:noscroll \
  http://download.savannah.gnu.org/releases/liquidwar6/0.6.3902/liquidwar6_0.6.3902-0vendor_i386.deb \
  && dpkg -i liquidwar6_0.6.3902-0vendor_i386.deb; \
apt install -f -y \
  && rm liquidwar6_0.6.3902-0vendor_i386.deb


# Warsow 2.1.2
RUN cd /tmp \
  && wget -nv --show-progress --progress=bar:force:noscroll \
  http://sebastian.network/warsow/warsow-2.1.2.tar.gz \
  && tar xzf warsow-2.1.2.tar.gz \
  && mv warsow-2.1.2 /opt/ \
  && rm warsow-2.1.2.tar.gz

# Ludimus node content
COPY ludimus-node /ludimus-node
