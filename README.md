# ludimus

**ludimus : let's play all together**

A set of tools to ease LAN parties setup and experiment digital collaborative entertainment.

From the latin word _ludimus_ which means _we play_.

# Prerequisites

- 64 bits linux box
- python 3
- docker CE >=18.09 (without sudo for the current user)
- GPU on /dev/dri
- sound card on /dev/snd

# Quickstart

```
wget -O - https://gitlab.com/ph.e/ludimus/raw/master/ludimus-bootstrap | bash
```

# Goals

- Docker GPU accelerated games
- Docker games with sound
- game servers in docker
- local cache to speed games install on local networks
- graphical frontend
- game start/stop automation
- overlay graphics
- support for linux steam games
- support for windows games via wine (toblo)

# Docker GPU accelerated games

```
sudo apt-get install glmark2
```

From my laptop with a Haswell-ULT Integrated Graphics Controller (without docker) :

```
=======================================================
    glmark2 2014.03+git20150611.fa71af2d
=======================================================
    OpenGL Information
    GL_VENDOR:     Intel Open Source Technology Center
    GL_RENDERER:   Mesa DRI Intel(R) Haswell Mobile
    GL_VERSION:    3.0 Mesa 18.2.2
=======================================================
[build] use-vbo=false: FPS: 2099 FrameTime: 0.476 ms
[build] use-vbo=true: FPS: 2171 FrameTime: 0.461 ms
[texture] texture-filter=nearest: FPS: 1990 FrameTime: 0.503 ms
[texture] texture-filter=linear: FPS: 2055 FrameTime: 0.487 ms
[texture] texture-filter=mipmap: FPS: 2070 FrameTime: 0.483 ms
[shading] shading=gouraud: FPS: 1811 FrameTime: 0.552 ms
[shading] shading=blinn-phong-inf: FPS: 1827 FrameTime: 0.547 ms
[shading] shading=phong: FPS: 1807 FrameTime: 0.553 ms
[shading] shading=cel: FPS: 1774 FrameTime: 0.564 ms
[bump] bump-render=high-poly: FPS: 1430 FrameTime: 0.699 ms
[bump] bump-render=normals: FPS: 2131 FrameTime: 0.469 ms
[bump] bump-render=height: FPS: 2106 FrameTime: 0.475 ms
[effect2d] kernel=0,1,0;1,-4,1;0,1,0;: FPS: 1459 FrameTime: 0.685 ms
[effect2d] kernel=1,1,1,1,1;1,1,1,1,1;1,1,1,1,1;: FPS: 701 FrameTime: 1.427 ms
[pulsar] light=false:quads=5:texture=false: FPS: 1630 FrameTime: 0.613 ms
[desktop] blur-radius=5:effect=blur:passes=1:separable=true:windows=4: FPS: 650 FrameTime: 1.538 ms
[desktop] effect=shadow:windows=4: FPS: 995 FrameTime: 1.005 ms
[buffer] columns=200:interleave=false:update-dispersion=0.9:update-fraction=0.5:update-method=map: FPS: 606 FrameTime: 1.650 ms
[buffer] columns=200:interleave=false:update-dispersion=0.9:update-fraction=0.5:update-method=subdata: FPS: 572 FrameTime: 1.748 ms
[buffer] columns=200:interleave=true:update-dispersion=0.9:update-fraction=0.5:update-method=map: FPS: 665 FrameTime: 1.504 ms
[ideas] speed=duration: FPS: 1337 FrameTime: 0.748 ms
[jellyfish] <default>: FPS: 1078 FrameTime: 0.928 ms
[terrain] <default>: FPS: 153 FrameTime: 6.536 ms
[shadow] <default>: FPS: 844 FrameTime: 1.185 ms
[refract] <default>: FPS: 269 FrameTime: 3.717 ms
[conditionals] fragment-steps=0:vertex-steps=0: FPS: 1976 FrameTime: 0.506 ms
[conditionals] fragment-steps=5:vertex-steps=0: FPS: 1960 FrameTime: 0.510 ms
[conditionals] fragment-steps=0:vertex-steps=5: FPS: 1975 FrameTime: 0.506 ms
[function] fragment-complexity=low:fragment-steps=5: FPS: 1975 FrameTime: 0.506 ms
[function] fragment-complexity=medium:fragment-steps=5: FPS: 1965 FrameTime: 0.509 ms
[loop] fragment-loop=false:fragment-steps=5:vertex-steps=5: FPS: 1959 FrameTime: 0.510 ms
[loop] fragment-steps=5:fragment-uniform=false:vertex-steps=5: FPS: 1961 FrameTime: 0.510 ms
[loop] fragment-steps=5:fragment-uniform=true:vertex-steps=5: FPS: 1944 FrameTime: 0.514 ms
=======================================================
                                  glmark2 Score: 1513
=======================================================
```

```
docker build -t registry.gitlab.com/ph.e/ludimus .
docker push registry.gitlab.com/ph.e/ludimus
```

```
xhost +
docker run \
  --rm -t -i \
  --volume=/tmp/.X11-unix:/tmp/.X11-unix \
  --device=/dev/dri:/dev/dri \
  --device /dev/snd \
  --env="DISPLAY=$DISPLAY" \
  registry.gitlab.com/ph.e/ludimus \
  /bin/bash
```

From my laptop with a Haswell-ULT Integrated Graphics Controller (in docker) :

```
=======================================================
    glmark2 2014.03+git20150611.fa71af2d
=======================================================
    OpenGL Information
    GL_VENDOR:     Intel Open Source Technology Center
    GL_RENDERER:   Mesa DRI Intel(R) Haswell Mobile
    GL_VERSION:    3.0 Mesa 18.2.2
=======================================================
[build] use-vbo=false: FPS: 2037 FrameTime: 0.491 ms
[build] use-vbo=true: FPS: 2138 FrameTime: 0.468 ms
[texture] texture-filter=nearest: FPS: 1973 FrameTime: 0.507 ms
[texture] texture-filter=linear: FPS: 2008 FrameTime: 0.498 ms
[texture] texture-filter=mipmap: FPS: 2036 FrameTime: 0.491 ms
[shading] shading=gouraud: FPS: 1798 FrameTime: 0.556 ms
[shading] shading=blinn-phong-inf: FPS: 1811 FrameTime: 0.552 ms
[shading] shading=phong: FPS: 1795 FrameTime: 0.557 ms
[shading] shading=cel: FPS: 1751 FrameTime: 0.571 ms
[bump] bump-render=high-poly: FPS: 1412 FrameTime: 0.708 ms
[bump] bump-render=normals: FPS: 2075 FrameTime: 0.482 ms
[bump] bump-render=height: FPS: 2077 FrameTime: 0.481 ms
[effect2d] kernel=0,1,0;1,-4,1;0,1,0;: FPS: 1402 FrameTime: 0.713 ms
[effect2d] kernel=1,1,1,1,1;1,1,1,1,1;1,1,1,1,1;: FPS: 677 FrameTime: 1.477 ms
[pulsar] light=false:quads=5:texture=false: FPS: 1571 FrameTime: 0.637 ms
[desktop] blur-radius=5:effect=blur:passes=1:separable=true:windows=4: FPS: 629 FrameTime: 1.590 ms
[desktop] effect=shadow:windows=4: FPS: 965 FrameTime: 1.036 ms
[buffer] columns=200:interleave=false:update-dispersion=0.9:update-fraction=0.5:update-method=map: FPS: 587 FrameTime: 1.704 ms
[buffer] columns=200:interleave=false:update-dispersion=0.9:update-fraction=0.5:update-method=subdata: FPS: 537 FrameTime: 1.862 ms
[buffer] columns=200:interleave=true:update-dispersion=0.9:update-fraction=0.5:update-method=map: FPS: 637 FrameTime: 1.570 ms
[ideas] speed=duration: FPS: 1295 FrameTime: 0.772 ms
[jellyfish] <default>: FPS: 1067 FrameTime: 0.937 ms
[terrain] <default>: FPS: 148 FrameTime: 6.757 ms
[shadow] <default>: FPS: 825 FrameTime: 1.212 ms
[refract] <default>: FPS: 260 FrameTime: 3.846 ms
[conditionals] fragment-steps=0:vertex-steps=0: FPS: 1775 FrameTime: 0.563 ms
[conditionals] fragment-steps=5:vertex-steps=0: FPS: 1807 FrameTime: 0.553 ms
[conditionals] fragment-steps=0:vertex-steps=5: FPS: 1770 FrameTime: 0.565 ms
[function] fragment-complexity=low:fragment-steps=5: FPS: 1810 FrameTime: 0.552 ms
[function] fragment-complexity=medium:fragment-steps=5: FPS: 1755 FrameTime: 0.570 ms
[loop] fragment-loop=false:fragment-steps=5:vertex-steps=5: FPS: 1770 FrameTime: 0.565 ms
[loop] fragment-steps=5:fragment-uniform=false:vertex-steps=5: FPS: 1793 FrameTime: 0.558 ms
[loop] fragment-steps=5:fragment-uniform=true:vertex-steps=5: FPS: 1804 FrameTime: 0.554 ms
=======================================================
                                  glmark2 Score: 1448
=======================================================
```

# Docker games with sound

We will play container sound on the host pulseaudio.

cf https://askubuntu.com/questions/972510/how-to-set-alsa-default-device-to-pulseaudio-sound-server-on-docker

We need to mount the sound device _/dev/snd_

```
docker run -it \
    --device /dev/snd \
    -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
    -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native \
    -v ~/.config/pulse/cookie:/root/.config/pulse/cookie \
    --group-add $(getent group audio | cut -d: -f3) \
    ubuntu:16.04 /bin/bash

apt install alsa-utils
aplay /usr/share/sounds/alsa/Front_Center.wav
```

# Game servers in docker

The easy part !

Simply install the server apps in the OCI image.

For exemple, the teeworlds server is jus `apt install -y teeworlds-server` to install
and `/usr/games/teeworlds-server` to start.

# References

- http://wiki.ros.org/docker/Tutorials/Hardware%20Acceleration
- http://gernotklingler.com/blog/docker-replaced-virtual-machines-chroots/
- https://github.com/mviereck/x11docker
- https://mrminimal.gitlab.io/2018/07/26/godot-dedicated-server-tutorial.html
- https://hub.docker.com/r/andrey01/steam/dockerfile

# Mount shared volume

keygen ..

```
mkdir /mnt/droplet <--replace "droplet" whatever you prefer
sshfs -o allow_other,defer_permissions root@xxx.xxx.xxx.xxx:/ /mnt/droplet
```
